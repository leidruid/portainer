# \RolesApi

All URIs are relative to *http://portainer.domain/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RoleList**](RolesApi.md#RoleList) | **Get** /roles | List roles


# **RoleList**
> RoleListResponse RoleList(ctx, )
List roles

List all roles available for use with the RBAC extension. **Access policy**: administrator 

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**RoleListResponse**](RoleListResponse.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

