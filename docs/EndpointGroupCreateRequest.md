# EndpointGroupCreateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Endpoint group name | [default to null]
**Description** | **string** | Endpoint group description | [optional] [default to null]
**Labels** | [**[]Pair**](Pair.md) |  | [optional] [default to null]
**AssociatedEndpoints** | **[]int32** | List of endpoint identifiers that will be part of this group | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


