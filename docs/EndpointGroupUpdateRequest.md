# EndpointGroupUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Endpoint group name | [optional] [default to null]
**Description** | **string** | Endpoint group description | [optional] [default to null]
**Tags** | **[]string** | List of tags associated to the endpoint group | [optional] [default to null]
**UserAccessPolicies** | [***UserAccessPolicies**](UserAccessPolicies.md) |  | [optional] [default to null]
**TeamAccessPolicies** | [***TeamAccessPolicies**](TeamAccessPolicies.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


