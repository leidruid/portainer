# TemplateEnvSelect

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | **string** | Some text that will displayed as a choice | [optional] [default to null]
**Value** | **string** | A value that will be associated to the choice | [optional] [default to null]
**Default_** | **bool** | Will set this choice as the default choice | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


