# Registry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | Registry identifier | [optional] [default to null]
**Name** | **string** | Registry name | [optional] [default to null]
**URL** | **string** | URL or IP address of the Docker registry | [optional] [default to null]
**Authentication** | **bool** | Is authentication against this registry enabled | [optional] [default to null]
**Username** | **string** | Username used to authenticate against this registry | [optional] [default to null]
**Password** | **string** | Password used to authenticate against this registry | [optional] [default to null]
**AuthorizedUsers** | **[]int32** | List of user identifiers authorized to use this registry | [optional] [default to null]
**AuthorizedTeams** | **[]int32** | List of team identifiers authorized to use this registry | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


