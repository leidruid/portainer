# TemplateRepository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**URL** | **string** | URL of a git repository used to deploy a stack template. Mandatory for a Swarm/Compose stack template | [default to null]
**Stackfile** | **string** | Path to the stack file inside the git repository | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


