# Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | Role identifier | [optional] [default to null]
**Name** | **string** | Role name | [optional] [default to null]
**Description** | **string** | Role description | [optional] [default to null]
**Authorizations** | [***Authorizations**](Authorizations.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


