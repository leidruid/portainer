# DockerHub

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Authentication** | **bool** | Is authentication against DockerHub enabled | [optional] [default to null]
**Username** | **string** | Username used to authenticate against the DockerHub | [optional] [default to null]
**Password** | **string** | Password used to authenticate against the DockerHub | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


