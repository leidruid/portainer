# DockerHubUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Authentication** | **bool** | Enable authentication against DockerHub | [default to null]
**Username** | **string** | Username used to authenticate against the DockerHub | [default to null]
**Password** | **string** | Password used to authenticate against the DockerHub | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


