# TlsConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TLS** | **bool** | Use TLS | [optional] [default to null]
**TLSSkipVerify** | **bool** | Skip the verification of the server TLS certificate | [optional] [default to null]
**TLSCACertPath** | **string** | Path to the TLS CA certificate file | [optional] [default to null]
**TLSCertPath** | **string** | Path to the TLS client certificate file | [optional] [default to null]
**TLSKeyPath** | **string** | Path to the TLS client key file | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


