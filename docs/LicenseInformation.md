# LicenseInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LicenseKey** | **string** | License key | [optional] [default to null]
**Company** | **string** | Company associated to the license | [optional] [default to null]
**Expiration** | **string** | License expiry date | [optional] [default to null]
**Valid** | **bool** | Is the license valid | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


