# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Authentication** | **bool** | Is authentication enabled | [optional] [default to null]
**EndpointManagement** | **bool** | Is endpoint management enabled | [optional] [default to null]
**Analytics** | **bool** | Is analytics enabled | [optional] [default to null]
**Version** | **string** | Portainer API version | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


