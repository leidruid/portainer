# UserCreateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | Username | [default to null]
**Password** | **string** | Password | [default to null]
**Role** | **int32** | User role (1 for administrator account and 2 for regular account) | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


