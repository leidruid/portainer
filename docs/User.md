# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | User identifier | [optional] [default to null]
**Username** | **string** | Username | [optional] [default to null]
**Password** | **string** | Password | [optional] [default to null]
**Role** | **int32** | User role (1 for administrator account and 2 for regular account) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


