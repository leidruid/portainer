# StackUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StackFileContent** | **string** | New content of the Stack file. | [optional] [default to null]
**Env** | [**[]StackEnv**](Stack_Env.md) | A list of environment variables used during stack deployment | [optional] [default to null]
**Prune** | **bool** | Prune services that are no longer referenced (only available for Swarm stacks) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


