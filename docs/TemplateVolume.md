# TemplateVolume

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Container** | **string** | Path inside the container | [optional] [default to null]
**Bind** | **string** | Path on the host | [optional] [default to null]
**Readonly** | **bool** | Whether the volume used should be readonly | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


