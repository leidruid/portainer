# Extension

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | Extension identifier | [optional] [default to null]
**Name** | **string** | Extension name | [optional] [default to null]
**Enabled** | **bool** | Is the extension enabled | [optional] [default to null]
**ShortDescription** | **string** | Short description about the extension | [optional] [default to null]
**DescriptionURL** | **string** | URL to the file containing the extension description | [optional] [default to null]
**Available** | **bool** | Is the extension available for download and activation | [optional] [default to null]
**Images** | **[]string** | List of screenshot URLs | [optional] [default to null]
**Logo** | **string** | Icon associated to the extension | [optional] [default to null]
**Price** | **string** | Extension price | [optional] [default to null]
**PriceDescription** | **string** | Details about extension pricing | [optional] [default to null]
**ShopURL** | **string** | URL used to buy the extension | [optional] [default to null]
**UpdateAvailable** | **bool** | Is an update available for this extension | [optional] [default to null]
**Version** | **string** | Extension version | [optional] [default to null]
**License** | [***LicenseInformation**](LicenseInformation.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


