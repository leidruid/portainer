# \StatusApi

All URIs are relative to *http://portainer.domain/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StatusInspect**](StatusApi.md#StatusInspect) | **Get** /status | Check Portainer status


# **StatusInspect**
> Status StatusInspect(ctx, )
Check Portainer status

Retrieve Portainer status. **Access policy**: public 

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**Status**](Status.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

